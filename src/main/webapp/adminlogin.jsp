<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
<style>
body {
	display: grid;
	justify-content: center;
	background-color: LightBlue;
}

.main {
	display: grid;
	border: 2px solid green;
	width: 400px;
	height: 400px;
	margin-top: 212px;
}

form {
	display: grid;
	justify-content: center;
	align-content: center;
	height: 100%;
	background-color: blueviolet;
}

label, input[type="text"], input[type="password"], input[type="submit"]
	{
	display: block;
	margin-bottom: 10px;
	width: 100%;
	padding: 5px;
	box-sizing: border-box;
}

input[type="submit"] {
	background-color: #4CAF50;
	color: white;
	border: none;
	padding: 10px;
	cursor: pointer;
	width: auto;
	margin: 10px auto;
}
</style>
</head>
<body>
	<div class="main">
		<form action="LoginServlet" method="POST">
			<h1 style="text-align: center;">Login</h1>
			<label for="username">Username:</label> <input type="text"
				id="username" name="username" required> <label
				for="password">Password:</label> <input type="password"
				id="password" name="password" required> <input type="submit"
				value="Login">
		</form>
	</div>
</body>
</html>