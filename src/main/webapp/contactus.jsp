<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Contact Us</title>
<style>
body {
	background-color: #333333;
}

.main {
	display: grid;
	justify-content: center;
}

.container {
	display: grid;
	justify-content: center;
	border: 1px solid white;
	width: 600px;
	background-color: #f8f8f8;
}

.section {
	border: none;
	gap: 1px;
	padding: 2px;
	margin: 2px;
	background-color: #e1f2fb;
	width: 598px;
}

h1, p {
	text-align: center;
}

.others {
	padding: 10px;
	margin: 30px;
}

.others h3 {
	margin-top: 10px;
}

.others input[type="text"], .others input[type="email"] {
	width: 100%;
	padding: 5px;
	margin-bottom: 10px;
}

input[type="submit"] {
	width: 150px;
	height: 40px;
	font-size: 18px;
}

.myclass::after {
	content: "*";
	color: red;
}
</style>
</head>
<body>
	<div class="main">
		<form action="ContactServlet" method="post">
			<div class="container">
				<div class="section">
					<h1 style="color: black;">Contact Us</h1>
					<p>Please fill the form in a decent manner</p>
				</div>

				<div class="others">
					<h3 class="myclass">Full Name</h3>
					<input type="text" id="name" name="name" required><br>

					<h3 class="myclass">E-Mail</h3>
					<input type="text" id="email" name="email" required> <span>example@gmail.com</span><br>

					<h3 class="myclass">Message</h3>
					<input type="text" id="message" name="message" required
						style="height: 88px;"><br>
				</div>

				<input type="submit" value="Submit"
					style="width: 72px; position: relative; left: 252px; bottom: 23px;">
			</div>
		</form>
	</div>
</body>
</html>
