<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>ContactUs Application</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: blue">
			<div>ContactUs Application</div>

			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link"></a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">List of Active Requests</h3>
			<hr>
			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>User Id</th>
						<th>Full Name</th>
						<th>Email</th>
						<th>Message</th>
						<th>Active_Status</th>
						<th>createdTime</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${activeRequests}">
						<form action="<%=request.getContextPath()%>/DashboardServlet"
							method="post">

							<tr>
								<td><c:out value="${user.id}" /></td>
								<td><c:out value="${user.fullName}" /></td>
								<td><c:out value="${user.email}" /></td>
								<td><c:out value="${user.message}" /></td>
								<td>true</td>
								<td><c:out value="${user.date}" /></td>
								<td><input type="hidden" name="requestId"
									value="<c:out value='${user.id}' />" />
									<button type="submit">make Archive</button></td>
							</tr>

						</form>
					</c:forEach>
				</tbody>
			</table>
			<h3 class="text-center">List of Archive Requests</h3>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>User Id</th>
						<th>Full Name</th>
						<th>Email</th>
						<th>Message</th>
						<th>Active_Status</th>
						<th>createdTime</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="user" items="${archiveRequests}">
						<form action="<%=request.getContextPath()%>/UpdateServlet"
							method="post">
							<tr>

								<td><c:out value="${user.id}" /></td>
								<td><c:out value="${user.fullName}" /></td>
								<td><c:out value="${user.email}" /></td>
								<td><c:out value="${user.message}" /></td>
								<td>false</td>
								<td><c:out value="${user.date}" /></td>
								<td><input type="hidden" name="requestId"
									value="<c:out value='${user.id}' />" />
									<button type="submit">make Active</button></td>

							</tr>
						</form>
					</c:forEach>
				</tbody>

			</table>
		</div>
	</div>
</body>
</html>