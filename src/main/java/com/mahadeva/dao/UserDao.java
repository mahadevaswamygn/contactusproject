package com.mahadeva.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mahadeva.model.User;

public class UserDao {
	public boolean checkCredentials(User user)
	{
		try {
			String username=user.getUsername();
			String password =user.getPassword();
			String adminQuery="select * from adminuser where username=? and password=?";
			PreparedStatement statement=Database.getConnection().prepareStatement(adminQuery);
			statement.setString(1, username);
			statement.setString(2, password);
			
			ResultSet adminResultset= statement.executeQuery();
			if(adminResultset.next())
			{
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
