package com.mahadeva.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mahadeva.model.Request;

public class RequestDao {
	public boolean saveRequest(Request request) {
		String query = "insert into requests (fullname,email,message) values(?,?,?)";
		PreparedStatement statement;
		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, request.getFullName());
			statement.setString(2, request.getEmail());
			statement.setString(3, request.getMessage());

			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Request> getActiveRequests() {
		List<Request> activeRequests = new ArrayList<>();
		String query = "select * from requests where flag=?";
		PreparedStatement statement;

		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, 1);
			ResultSet activeRequestsResult = statement.executeQuery();

			while (activeRequestsResult.next()) {
				Request request = new Request();
				request.setId(activeRequestsResult.getInt("id"));
				request.setFullName(activeRequestsResult.getString("fullname"));
				request.setEmail(activeRequestsResult.getString("email"));
				request.setMessage(activeRequestsResult.getString("message"));
				request.setIsActive(activeRequestsResult.getInt("flag"));
				request.setDate(activeRequestsResult.getString("created_at"));

				activeRequests.add(request);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return activeRequests;
	}

	public List<Request> getArchiveRequests() {
		List<Request> acrhiveRequests = new ArrayList<>();
		String query = "select * from requests where flag=?";
		PreparedStatement statement;

		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, 0);
			ResultSet archivedRequestResult = statement.executeQuery();

			while (archivedRequestResult.next()) {
				Request request = new Request();
				request.setId(archivedRequestResult.getInt("id"));
				request.setFullName(archivedRequestResult.getString("fullname"));
				request.setEmail(archivedRequestResult.getString("email"));
				request.setMessage(archivedRequestResult.getString("message"));
				request.setIsActive(archivedRequestResult.getInt("flag"));
				request.setDate(archivedRequestResult.getString("created_at"));

				acrhiveRequests.add(request);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return acrhiveRequests;
	}

	public boolean updateStatusOfRequestToArchive(int id) {
		String query = "update requests set flag=? where id=?";
		PreparedStatement statement;

		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, 0);
			statement.setInt(2, id);

			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateStatusOfRequestToActive(int id) {
		String query = "update requests set flag=? where id=?";
		PreparedStatement statement;

		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, 1);
			statement.setInt(2, id);

			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public Request findRequestById(int requestId) {
		String query = "select * from requests where id=?";
		PreparedStatement statement;
		Request request = new Request();

		try {
			Connection connection = Database.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, requestId);

			ResultSet requestReslut = statement.executeQuery();

			while (requestReslut.next()) {
				request.setId(requestReslut.getInt("id"));
				request.setFullName(requestReslut.getString("fullname"));
				request.setEmail(requestReslut.getString("email"));
				request.setMessage(requestReslut.getString("message"));
				request.setIsActive(requestReslut.getInt("flag"));
				request.setDate(requestReslut.getString("created_at"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return request;
	}
}
