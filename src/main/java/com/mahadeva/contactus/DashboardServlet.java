package com.mahadeva.contactus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mahadeva.dao.RequestDao;
import com.mahadeva.model.Request;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDao requestDao = new RequestDao();
		List <Request> activeRequests = requestDao.getActiveRequests();
		List <Request> archiveRequests = requestDao.getArchiveRequests();
		request.setAttribute("archiveRequests", archiveRequests);
		request.setAttribute("activeRequests", activeRequests);
		RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest servletRequest , HttpServletResponse response) throws ServletException, IOException {
		int requestId = Integer.parseInt(servletRequest.getParameter("requestId"));
		RequestDao requestDao= new RequestDao();
		Request request=requestDao.findRequestById(requestId);
		if(request.getIsActive()==1) {
			requestDao.updateStatusOfRequestToArchive(requestId);
			response.sendRedirect("dashboardt");
		}
		else
		{
			requestDao.updateStatusOfRequestToActive(requestId);
			response.sendRedirect("dashboard");
		}	
	}
}
