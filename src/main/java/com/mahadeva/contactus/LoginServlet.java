package com.mahadeva.contactus;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mahadeva.dao.UserDao;
import com.mahadeva.model.User;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("adminlogin.jsp");
		requestDispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		UserDao userDao = new UserDao();
		Boolean isValidUser =  userDao.checkCredentials(user);
		if(isValidUser) {
			response.sendRedirect("dashboard");
		}
		else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("adminlogin.jsp");
			requestDispatcher.forward(request, response);
		}
	}
}
