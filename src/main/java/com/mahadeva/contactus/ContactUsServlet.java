package com.mahadeva.contactus;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mahadeva.dao.RequestDao;
import com.mahadeva.model.Request;


@WebServlet("/contactus")
public class ContactUsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher=request.getRequestDispatcher("contactus.jsp");
		requestDispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
		String fullName = servletRequest.getParameter("name");
		String email = servletRequest.getParameter("email");
		String message = servletRequest.getParameter("message");
		Request request = new Request();
		request.setFullName(fullName);
		request.setEmail(email);
		request.setMessage(message);
		RequestDao requestDao = new RequestDao();
		requestDao.saveRequest(request);
		servletResponse.getWriter().print("<h1>We well  contact to you soon...!</h1>");
	}
    
}
